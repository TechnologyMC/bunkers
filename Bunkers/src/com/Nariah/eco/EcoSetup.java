package com.Nariah.eco;

import org.bukkit.plugin.RegisteredServiceProvider;

import com.Nariah.Bunkers;

import net.milkbowl.vault.economy.Economy;

public class EcoSetup {
	
    static Bunkers main;
	
	public EcoSetup(Bunkers plugin) {
		main = plugin;
	}
	
	public static Economy economy = null;
	
	public static boolean setupEconomy() {
		RegisteredServiceProvider<Economy> economyProvider = main.getServer().getServicesManager()
				.getRegistration(net.milkbowl.vault.economy.Economy.class);
		if (economyProvider != null) {
			economy = economyProvider.getProvider();
		}

		return (economy != null);
	}
	
}
