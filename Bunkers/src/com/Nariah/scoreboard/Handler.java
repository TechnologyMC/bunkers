package com.Nariah.scoreboard;

import java.util.HashMap;
import java.util.Map;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.event.player.PlayerKickEvent;
import org.bukkit.event.player.PlayerQuitEvent;
import org.bukkit.scoreboard.Scoreboard;

import com.Nariah.Bunkers;

public class Handler extends Manage implements Listener {
	
	Bunkers main;

	public Handler(Bunkers plugin) {
		super(plugin);
		
		main = plugin;
	}

	public static Map<Player, Helper> helpers = new HashMap<>();

	protected void init() {
		getPlugin().getServer().getPluginManager().registerEvents(this, getPlugin());
		
		for (Player player : Bukkit.getOnlinePlayers()) {
			handleJoin(player);
		}
	}

	public static Helper getScoreboardFor(Player player) {
		return helpers.get(player);
	}

	public void handleJoin(Player player) {
		Scoreboard bukkitScoreBoard = getPlugin().getServer().getScoreboardManager().getNewScoreboard();
		player.setScoreboard(bukkitScoreBoard);
		Helper helper = new Helper(bukkitScoreBoard,
				ChatColor.translateAlternateColorCodes('&', "&6&lValux&7 (Bunkers)"));
		helpers.put(player, helper);
	}

	@EventHandler
	public void onPlayerJoin(PlayerJoinEvent e) {
		handleJoin(e.getPlayer());
	}

	@EventHandler
	public void onPlayerKick(PlayerKickEvent e) {
		helpers.remove(e.getPlayer());
	}

	@EventHandler
	public void onPlayerQuit(PlayerQuitEvent e) {
		helpers.remove(e.getPlayer());
	}
}
