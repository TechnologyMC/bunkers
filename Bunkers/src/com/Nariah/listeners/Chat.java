package com.Nariah.listeners;

import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.AsyncPlayerChatEvent;

import com.Nariah.game.GameCmd;
import com.Nariah.utils.BooleanManager;

import net.md_5.bungee.api.ChatColor;

public class Chat implements Listener {

	@EventHandler
	public void onChat(AsyncPlayerChatEvent e) {
		Player p = e.getPlayer();

		if (GameCmd.time != 0) {
			e.setCancelled(true);
			p.sendMessage(ChatColor.RED + "You may not speak until the game has started.");
		}

		if (BooleanManager.chatMuted) {
			if (!p.hasPermission("Tribes.staff")) {
				e.setCancelled(true);
				p.sendMessage(ChatColor.RED + "Chat is currently muted, please try again later...");
			} else {
				e.setCancelled(false);
			}
		}

	}

}
