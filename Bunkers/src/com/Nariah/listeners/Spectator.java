package com.Nariah.listeners;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.UUID;

import org.bukkit.Bukkit;
import org.bukkit.GameMode;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.PlayerDeathEvent;
import org.bukkit.event.player.PlayerDropItemEvent;

import net.md_5.bungee.api.ChatColor;

public class Spectator implements Listener {

	Map<String, Integer> deaths = new HashMap<String, Integer>();
	HashSet<UUID> spectator = new HashSet<>();

	public void spectatorMode(Player p) {

		// Vanish player etc...
		for (Player players : Bukkit.getOnlinePlayers()) {
			players.hidePlayer(p);
		}
		p.getInventory().clear();
		p.getInventory().setArmorContents(null);

		p.setGameMode(GameMode.CREATIVE);
		p.sendMessage(ChatColor.RED + "You have been put in spectator mode as you have died 5 times...");

	}

	@EventHandler
	public void onDeath(PlayerDeathEvent e) {
		Player p = (Player) e.getEntity();

		if (!deaths.containsKey(p.getName())) {

			deaths.put(p.getName(), 1);

		} else {
			deaths.put(p.getName(), deaths.get(e.getEntity().getName()) + 1);
		}

		if (deaths.get(p.getName()) == 5) {

			spectator.add(p.getUniqueId());
			spectatorMode(p);
		}
	}

	@EventHandler
	public void onDrop(PlayerDropItemEvent e) {
		Player p = e.getPlayer();

		if (spectator.contains(p.getUniqueId())) {
			e.setCancelled(true);
		}

	}

}
