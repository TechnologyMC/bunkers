package com.Nariah.listeners;

import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.PlayerDeathEvent;

import com.Nariah.Bunkers;
import com.Nariah.eco.EcoSetup;

import net.md_5.bungee.api.ChatColor;

public class KillMoney implements Listener {

	Bunkers main;

	public KillMoney(Bunkers plugin) {
		main = plugin;
	}

	@EventHandler
	public void onHit(PlayerDeathEvent e) {
		if (e.getEntity().getKiller() instanceof Player) {
			
			e.getEntity().getKiller().sendMessage(ChatColor.GOLD + "You have recieved " + ChatColor.BOLD + "$100 "
					+ ChatColor.GOLD + "for killing " + ChatColor.RED + e.getEntity().getKiller().getName());
			//Send player money.
			EcoSetup.economy.depositPlayer(e.getEntity().getKiller(), 100D);
		}

	}

}
