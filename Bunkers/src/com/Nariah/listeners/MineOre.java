package com.Nariah.listeners;

import org.bukkit.Material;
import org.bukkit.block.Block;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.BlockBreakEvent;
import org.bukkit.inventory.ItemStack;

import com.Nariah.Bunkers;

public class MineOre implements Listener {

	Bunkers main;

	public MineOre(Bunkers plugin) {
		main = plugin;
	}

	@EventHandler
	public void onBreak(BlockBreakEvent e) {
		Block b = e.getBlock();
		Player p = e.getPlayer();

		if (b.equals(Material.IRON_ORE)) {
			b.setType(Material.COBBLESTONE);

			if (b.equals(Material.IRON_ORE)) {
				p.getInventory().addItem(new ItemStack(Material.IRON_INGOT));
			}
		}

		this.main.getServer().getScheduler().scheduleSyncDelayedTask(this.main, new Runnable() {

			@Override
			public void run() {

				b.setType(Material.IRON_ORE);

			}

		}, 5 * 20L);
	}

}
