package com.Nariah.staff.commands;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;

public class ClearChat implements CommandExecutor {

	public boolean onCommand(CommandSender sender, Command command, String label, String[] args) {
		if (!sender.hasPermission("StaffUtils.staff")) {
			sender.sendMessage(ChatColor.RED + "No Permission.");
			return true;
		}

		for (int i = 0; i < 225; i++) {
			Bukkit.broadcastMessage(" ");
		}
		Bukkit.broadcastMessage(ChatColor.GREEN + "Chat has been cleared by " + sender.getName());

		return true;

	}
}
