package com.Nariah.staff.commands;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;

import com.Nariah.utils.BooleanManager;


public class MuteChat implements CommandExecutor {

	public boolean onCommand(CommandSender sender, Command command, String label, String[] args) {

		if (!sender.hasPermission("StaffUtils.staff")) {
			sender.sendMessage(ChatColor.RED + "No Permission.");
			return true;
		}
		
		if (BooleanManager.chatMuted) {
			Bukkit.broadcastMessage(ChatColor.GREEN + "Chat has been locked by " + sender.getName());
			BooleanManager.chatMuted = false;

		} else {
			BooleanManager.chatMuted = true;
			Bukkit.broadcastMessage(ChatColor.GREEN + "Chat has been unlocked by " + sender.getName());
		}
		return true;

	}
}
