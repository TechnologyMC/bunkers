package com.Nariah.game;

import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import com.Nariah.Bunkers;
import com.Nariah.teams.Blue;
import com.Nariah.teams.Red;

import net.md_5.bungee.api.ChatColor;

public class GameCmd implements CommandExecutor {

	Bunkers main;

	public GameCmd(Bunkers plugin) {

		plugin = main;
	}

	public static int time = 30;
    int task;
	
	public boolean onCommand(CommandSender sender, Command command, String label, String[] args) {

		if (!sender.hasPermission("")) {
			sender.sendMessage(ChatColor.RED + "No Permission.");
			return true;
		}

		if (args.length < 1) {
			sender.sendMessage(ChatColor.RED + "Usage: /game {start/stop}");
			return true;
		}

		if (args.length == 1) {
			Player p = (Player) sender;

			if (args[0].equalsIgnoreCase("start")) {
				if (Blue.blueTeam.size() == 10 && Red.redTeam.size() == 10) {
					this.gameStart(p);
				} else {
					sender.sendMessage(ChatColor.RED + "Error: Not enough players to start the game.");
					return true;
				}
			} else if(args[0].equalsIgnoreCase("stop")) {
				Bukkit.getServer().getScheduler().cancelTask(task);
			}

		}

		return true;
	}

	private void gameStart(Player p) {
		Location blue = new Location(p.getWorld(), 100, 100, 100); // edit to
																	// base
																	// coordinates
		Location red = new Location(p.getWorld(), 110, 100, 110); // ^

		task = this.main.getServer().getScheduler().scheduleSyncRepeatingTask(this.main, new Runnable() {

			public void run() {

				time--;
				if (time != 0) {
					Bukkit.broadcastMessage(ChatColor.YELLOW + "Game starting in " + ChatColor.GOLD + time);
				}
				if (time == 0) {
					for (Player players : Bukkit.getOnlinePlayers()) {
						if (Blue.blueTeam.contains(players.getUniqueId())) {
							players.teleport(blue);
						} else {
							players.teleport(red);
						}
					}
					Bukkit.broadcastMessage(ChatColor.GREEN + "Game starting...");

				}

			}
		}, 0L, 30 * 20L);

	}

}
