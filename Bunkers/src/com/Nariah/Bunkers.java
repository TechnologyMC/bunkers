package com.Nariah;

import org.bukkit.Bukkit;
import org.bukkit.plugin.PluginManager;
import org.bukkit.plugin.java.JavaPlugin;
import com.Nariah.eco.EcoSetup;
import com.Nariah.game.GameCmd;
import com.Nariah.listeners.Chat;
import com.Nariah.listeners.Join;
import com.Nariah.listeners.KillMoney;
import com.Nariah.listeners.MineOre;
import com.Nariah.listeners.Spectator;
import com.Nariah.scoreboard.Handler;
import com.Nariah.staff.commands.ClearChat;
import com.Nariah.staff.commands.MuteChat;

public class Bunkers extends JavaPlugin {


	public void onEnable() {
		registerCommands();
		registerListeners();

		EcoSetup.setupEconomy();
	}

	private void registerCommands() {
		getCommand("game").setExecutor(new GameCmd(this));
		getCommand("clearchat").setExecutor(new ClearChat());
		getCommand("mutechat").setExecutor(new MuteChat());
	}

	private void registerListeners() {
		PluginManager pm = Bukkit.getPluginManager();

		pm.registerEvents(new KillMoney(this), this);
		pm.registerEvents(new Chat(), this);
		pm.registerEvents(new MineOre(this), this);
		pm.registerEvents(new Join(), this);
		pm.registerEvents(new Spectator(), this);
		pm.registerEvents(new Handler(this), this);
	}

}
